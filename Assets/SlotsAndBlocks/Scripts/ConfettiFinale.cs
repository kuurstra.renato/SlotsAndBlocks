﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfettiFinale : MonoBehaviour
{
    ParticleSystem particles;

    private void Awake()
    {
        particles = GetComponent<ParticleSystem>();
    }

    public void StopAndClear()
    {
        particles.Stop(false, ParticleSystemStopBehavior.StopEmittingAndClear);
    }

    public void Stop()
    {
        particles.Stop();
    }



    public void Play( )
    {
        Invoke("PlayDelay", Random.Range(0f, 0.3f));
    }


    public void PlayDelay()
    {
        particles.Play();
        Invoke("Stop", Random.Range(0f, 0.3f));
    }
}
