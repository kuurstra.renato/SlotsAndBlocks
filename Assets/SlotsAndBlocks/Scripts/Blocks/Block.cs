﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivableBlck
{
    // Add clips and a audiosource, and one of those clip will be played at random
    // while the game is active
    public class Block : MonoBehaviour
    {
        Vector3 _initialPosition;
        Quaternion _initialRotation;

        protected Rigidbody _rb;
        protected bool _IsPlaying = false;

        [Header("Random Sounds parameters")]
        public AudioClip[] RandomSoundsClip;
        public float probability = 0.2f;
        public float frequency;

        public bool IsPlayingRandomSounds = false;

        WaitForSeconds randomSoundsFrequency;
        WaitForSeconds RandomDelay;
        protected AudioSource audioSource;

        private void Awake()
        {
            _rb = GetComponent<Rigidbody>();

            if (_rb != null)
            {
                _rb.isKinematic = true;
            }
            randomSoundsFrequency = new WaitForSeconds(frequency);
            RandomDelay = new WaitForSeconds(Random.Range(0f, frequency));
            audioSource = GetComponent<AudioSource>();

			InitializePosition();
        }

        virtual public void Play()
        {
            _IsPlaying = true;
            if (_rb != null)
            {
                _rb.isKinematic = false;

            }

            if (IsPlayingRandomSounds)
            {
                StartCoroutine(PlayRandomSounds());
            }
        }

        virtual public void Stop()
        {
            _IsPlaying = false;

            transform.position = _initialPosition;
            transform.rotation = _initialRotation;

            if (_rb != null)
            {
                _rb.velocity = Vector3.zero;
                _rb.isKinematic = true;
            }

            StopAllCoroutines();
        }

        protected IEnumerator PlayRandomSounds()
        {
            yield return RandomDelay;
            while (true)
            {
                if (Random.Range(0f, 1.0f) >= probability)
                {
                    PlaySound(RandomSoundsClip[Random.Range(0, RandomSoundsClip.Length)]);
                }
                yield return randomSoundsFrequency;
            }
        }

        protected void PlaySound(AudioClip clip)
        {
            audioSource.clip = clip;
            audioSource.pitch = Random.Range(0.95f, 1.05f);
            audioSource.volume = 0.3f*Random.Range(0.95f, 1.05f);
            audioSource.Play();
        }

        virtual public void InitializePosition()
        {
            _initialPosition = transform.position;
            _initialRotation = transform.rotation;
        }
    }
}

