﻿using UnityEngine;

/// <summary>
/// Predetermined position for a block
/// </summary>
public class Slot : MonoBehaviour
{
	[SerializeField]
	TextMesh _emptyIndicator;

	GameObject _currentBlock;

	public GameObject GetCurrentBlock()
	{
		return _currentBlock;
	}

	public void OnSelect()
	{
		Debug.Log("Look at " + name);
	}

	public void OnDeselect()
	{
		Debug.Log("Stop looking at " + name);
	}

	private void Update()
	{
		if(_currentBlock == null)
		{
			_emptyIndicator.color = new Color(0, Mathf.Sin(16f * Time.realtimeSinceStartup), 0);
		}
	}

	public void SetBlock(GameObject prefab)
	{
		if(_currentBlock)
		{
			Destroy(_currentBlock);
		}

		if (prefab != null)
		{
			var block = Instantiate(prefab, transform);
			block.transform.localRotation = Quaternion.identity;
			block.transform.localPosition = new Vector3();
			_currentBlock = block;
		}
		else
			_currentBlock = null;

		_emptyIndicator.gameObject.SetActive(_currentBlock == null);
	}
}
