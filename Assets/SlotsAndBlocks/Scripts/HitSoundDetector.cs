﻿using UnityEngine;

public class HitSoundDetector : MonoBehaviour
{
	[SerializeField]
	AudioClip _hitSound;

	AudioSource _audio;
	Rigidbody _rb;

	float _previousVelocity;

	private void Awake()
	{
		_audio = GetComponent<AudioSource>();

		_rb = GetComponent<Rigidbody>();
	}

	private void Update()
	{
		float velocity = _rb.velocity.magnitude;
		float diff = Mathf.Abs(velocity - _previousVelocity);

		if (diff > 0.5f)
		{
			PlayHit();
		}

		// Test
		//if(Input.GetKeyDown(KeyCode.B))
		//{
		//	PlayHit();
		//}

		_previousVelocity = velocity;
	}

	void PlayHit()
	{
		_audio.clip = _hitSound;
		_audio.Play();
	}
}

