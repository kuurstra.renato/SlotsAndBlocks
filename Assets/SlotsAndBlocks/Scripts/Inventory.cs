﻿using UnityEngine;

[System.Serializable]
public class InventorySlot
{
	public GameObject prefab;
	public int initialAmount = 1;

	[HideInInspector]
	public int currentAmount;
}

public class Inventory : MonoBehaviour
{
	public InventorySlot[] inventory;
	public InventoryView view;
	public AudioClip nextSound;
	public AudioClip prevSound;

	[HideInInspector]
	public InventorySlot currentBlock;
	int _currentIndex;
	AudioSource _audio;

	public int currentIndex { get { return _currentIndex; } }

	public int blockCount { get { return inventory.Length; } }

	public InventorySlot GetSlot(int i)
	{
		return inventory[i];
	}

	// Must happen before the player initializes... eek
	void Awake()
	{
		_audio = GetComponent<AudioSource>();

		foreach(var slot in inventory)
		{
			slot.currentAmount = slot.initialAmount;
		}
		currentBlock = inventory[_currentIndex];
		view.Setup(this);
	}

	public void NextBlock()
	{
		++_currentIndex;
		if (_currentIndex >= inventory.Length)
			_currentIndex = 0;
		currentBlock = inventory[_currentIndex];

		view.OnCurrentBlockChanged();

		_audio.clip = nextSound;
		_audio.Play();
	}

	public void PrevBlock()
	{
		--_currentIndex;
		if (_currentIndex < 0)
			_currentIndex = inventory.Length - 1;
		currentBlock = inventory[_currentIndex];

		view.OnCurrentBlockChanged();

		_audio.clip = prevSound;
		_audio.Play();
	}
}
