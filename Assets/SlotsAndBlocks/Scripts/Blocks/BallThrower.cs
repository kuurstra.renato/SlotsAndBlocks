﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivableBlck
{
    public class BallThrower : Block
    {
        Collider _pushCollider;
        public GameObject Ball;
        public float Force = 250f;
        public Transform Arrow;

        private void Start()
        {
            _pushCollider = GetComponent<Collider>();
        }

        public override void Play()
        {
            base.Play();
            Ball.GetComponent<Rigidbody>().isKinematic = true;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (_IsPlaying == true)
            {
                Ball.GetComponent<Rigidbody>().isKinematic = false;
                Ball.GetComponent<Rigidbody>().AddForce(Arrow.transform.forward * Force);
            }
        }
    }
}
