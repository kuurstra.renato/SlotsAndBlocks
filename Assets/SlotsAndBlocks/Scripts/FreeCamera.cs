﻿using UnityEngine;

public class FreeCamera : MonoBehaviour
{
	public float sensitivity = 1f;
	public Transform pivot;
	public float minPitch = -89f;
	public float maxPitch = 89f;
	public float speed = 16f;
	//private Camera _cam;
	private float _pitch;
	private float _yaw;

	void Start()
	{
		//_cam = camera;
		if(pivot == null)
			pivot = this.transform;
	}
	
	void Update()
	{
		if(Cursor.lockState != CursorLockMode.None)
		{
			// Rotation

			float ax = Input.GetAxis("Mouse X");
			float ay = Input.GetAxis("Mouse Y");

			_yaw = transform.localEulerAngles.y + ax * sensitivity;
			
			_pitch += ay * sensitivity;
			_pitch = Mathf.Clamp(_pitch, minPitch, maxPitch);
			
			pivot.localEulerAngles = new Vector3(-_pitch, _yaw, 0f);

			// Position

			Vector3 motor = new Vector3();

			if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.Q))
			{
				motor.x -= 1f;
			}
			if(Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
			{
				motor.x += 1f;
			}
			if(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W) || Input.GetKey (KeyCode.Z))
			{
				motor.z += 1f;
			}
			if(Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
			{
				motor.z -= 1f;
			}
			if (Input.GetKey(KeyCode.Space))
			{
				motor.y += 1f;
			}
			if (Input.GetKey(KeyCode.LeftShift))
			{
				motor.y -= 1f;
			}

			motor.Normalize();

			var forward = transform.forward;
			forward = new Vector3(forward.x, 0, forward.z).normalized;

			float delta = Time.deltaTime / Time.timeScale;

			Vector3 pos = transform.position;
			pos += (motor.x * delta * speed) * transform.right;
			pos += (motor.z * delta * speed) * forward;
			pos += (motor.y * delta * speed) * Vector3.up;
			transform.position = pos;
		}
	}

}


