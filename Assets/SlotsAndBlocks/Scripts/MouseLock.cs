using UnityEngine;

/// <summary>
/// Locks the mouse if the player clicks on the screen.
/// The player will be able to get the mouse again by pressing ESC.
/// </summary>
public class MouseLock : MonoBehaviour
{
	void Update ()
	{
		if(Input.GetMouseButtonDown(0))
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
		}
		
		if(Input.GetKeyDown("escape"))
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;
		}
	}
	
}

