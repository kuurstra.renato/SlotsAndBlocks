﻿using ActivableBlck;
using UnityEngine;

//[RequireComponent(typeof(Inventory))]
public class PlayerInteraction : MonoBehaviour
{
	const float REACH_DISTANCE = 10f;

	Inventory _inventory;
	SimulationTrigger _simTrigger;
	Slot _pointedSlot;

	GameObject _freeBlock;
	Quaternion _freeRotation;
	float _freeBlockOffsetY = 1f;

	AudioSource _audio;
	[SerializeField] AudioClip placeSound;
	[SerializeField] AudioClip removeSound;
	[SerializeField] AudioClip rotateSound;

	private void Awake()
	{
		_audio = GetComponent<AudioSource>();
	}

	private void Start()
	{
		_inventory = GetComponent<Inventory>();
		_simTrigger = GetComponent<SimulationTrigger>();

		SetFreeBlock(_inventory.currentBlock.prefab);
	}

	private void Update()
	{
		var forward = transform.forward;

		// Detect slot
		{
			RaycastHit hit;
			Slot slot = null;
			if (Physics.Raycast(new Ray(transform.position, forward), out hit, REACH_DISTANCE, GameLayers.TrackSlot))
			{
				slot = hit.collider.GetComponent<Slot>();
			}
			SetCurrentSlot(slot);
		}

		bool place = Input.GetMouseButtonDown(0);
		bool remove = Input.GetMouseButtonDown(1);
		bool prevBlock = Input.GetAxis("Mouse ScrollWheel") > 0f;
		bool nextBlock = Input.GetAxis("Mouse ScrollWheel") < 0f;
		bool rotate = Input.GetKeyDown(KeyCode.R);
		bool start = Input.GetKeyDown(KeyCode.Return);
		bool stop = Input.GetKeyDown(KeyCode.Backspace);
		bool offsetUp = Input.GetKeyDown(KeyCode.KeypadPlus);
		bool offsetDown = Input.GetKeyDown(KeyCode.KeypadMinus);

		if (!_simTrigger.isSimulating)
		{
			if (nextBlock)
			{
				_inventory.NextBlock();
			}
			if (prevBlock)
			{
				_inventory.PrevBlock();
			}

			if (_pointedSlot != null)
			{
				// Slot interaction // <-- we started with this system, but now we could nuke it

				if (_freeBlock != null)
					_freeBlock.SetActive(false);

				if (place)
				{
					_pointedSlot.SetBlock(_inventory.currentBlock.prefab);
				}
				if (remove)
				{
					// Remove
					_pointedSlot.SetBlock(null);
				}
				if (rotate)
				{
					var block = _pointedSlot.GetCurrentBlock();
					block.transform.Rotate(new Vector3(0, 90, 0));
				}
				if (nextBlock || prevBlock)
				{
					_pointedSlot.SetBlock(_inventory.currentBlock.prefab);
				}
			}
			else
			{
				// Free interaction

				if (nextBlock || prevBlock)
				{
					SetFreeBlock(_inventory.currentBlock.prefab);
				}

				// could be removed if stacking worked properly :p
				if(offsetUp)
				{
					_freeBlockOffsetY += 0.25f;
				}
				if(offsetDown)
				{
					_freeBlockOffsetY -= 0.25f;
				}

				_freeBlock.SetActive(true);

				RaycastHit hit;
				if(Physics.Raycast(new Ray(transform.position, transform.forward), out hit, 10f))
				{
					_freeBlock.transform.position = hit.point + new Vector3(0, _freeBlockOffsetY, 0);
					_freeBlock.SetActive(true);

					if(remove)
					{
						var block = Util.GetTagInParent(hit.collider.gameObject, "Blocks");
						if (block != null)
						{
							Destroy(block);
							_audio.clip = removeSound;
							_audio.Play();
						}
					}
				}
				else
				{
					_freeBlock.SetActive(false);
				}

				if(rotate)
				{
					_freeBlock.transform.Rotate(new Vector3(0, 90, 0));
					_freeRotation = _freeBlock.transform.localRotation;
					_audio.clip = rotateSound;
					_audio.Play();
				}
				if(place && _freeBlock.activeSelf)
				{
					PlaceFreeBlock();
				}
			}
		}

		{
			if (start)
			{
				if (_freeBlock != null)
					_freeBlock.SetActive(false);

				_simTrigger.TogglePlay();

				_inventory.view.SetVisible(!_simTrigger.isSimulating);
			}
			if(stop)
			{
				if (_freeBlock != null)
					_freeBlock.SetActive(true);

				_simTrigger.Stop();
			}
		}
	}

	void SetFreeBlock(GameObject prefab)
	{
		if (_freeBlock != null)
			Destroy(_freeBlock);
		if(prefab != null)
		{
			_freeBlock = Instantiate(_inventory.currentBlock.prefab);
			_freeBlock.transform.localRotation = _freeRotation;
		}
		if (_freeBlock != null)
		{
			Util.SetLayerRecursively(_freeBlock, LayerMask.NameToLayer("Ignore Raycast"));
		}
	}

	void PlaceFreeBlock()
	{
		if (_freeBlock == null)
			return;

		var b = _freeBlock.GetComponent<Block>();
		if(b != null)
			b.InitializePosition();

		Util.SetLayerRecursively(_freeBlock, LayerMask.NameToLayer("Default"));
		_freeBlock = null;
		SetFreeBlock(_inventory.currentBlock.prefab);

		_audio.clip = placeSound;
		_audio.Play();
	}

	void SetCurrentSlot(Slot slot)
	{
		//Debug.Log(_pointedSlot + ", " + slot);
		if (_pointedSlot != slot)
		{
			// Slot changed

			if (_pointedSlot != null)
				_pointedSlot.OnDeselect();

			_pointedSlot = slot;

			if (_pointedSlot != null)
				_pointedSlot.OnSelect();
		}
	}
}

