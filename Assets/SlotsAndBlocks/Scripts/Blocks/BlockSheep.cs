﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivableBlck
{
    [RequireComponent(typeof(AudioSource), typeof(SphereCollider))]
    public class BlockSheep : Block
    {
        public AudioClip[] Activated;
        public AudioClip SheepBoom;
        SphereCollider _sphereCollider;
        bool _activated = false;
        public GameObject Explosion_ParticleEffect;
        public float ExplosionForce = 100f;
        public float explosionRadius = 10f;
        public float MovementTick = 0.05f;
        public float Speed = 2f;

        WaitForSeconds _waitForMovement;

        HashSet<Collider> _closeBeforePlay = new HashSet<Collider>();

        public void Start()
        {
            _sphereCollider = GetComponent<SphereCollider>();
            _waitForMovement = new WaitForSeconds(MovementTick);
        }

        public override void Play()
        {
            base.Play();
        }

        public override void Stop()
        {
            base.Stop();
            _activated = false;
            transform.GetChild(0).gameObject.SetActive(true);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag != "Blocks")
            {
                return;
            }

            if (_IsPlaying == false)
            {
                return;
            }

            _activated = !_activated;
            if (_activated == true)
            {
                PlaySound(Activated[Random.Range(0, Activated.Length)]);
                StartCoroutine(MoveForward());
            }
            else
            {
                Invoke("Explode", 1.0f);
            }
        }

        IEnumerator MoveForward()
        {
            while (gameObject.activeSelf)
            {
                yield return _waitForMovement;
                float speedChange = Speed - _rb.velocity.magnitude;
                _rb.AddForce(gameObject.transform.forward * Speed, ForceMode.VelocityChange);
            }
        }

        private void Explode()
        {
            GameObject go = GameObject.Instantiate(Explosion_ParticleEffect);
            _rb.AddExplosionForce(ExplosionForce, gameObject.transform.position, explosionRadius);
            go.transform.position = gameObject.transform.position;
            transform.GetChild(0).gameObject.SetActive(false);
            PlaySound(SheepBoom);
        }
    }
}

