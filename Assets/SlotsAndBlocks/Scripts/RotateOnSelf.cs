﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateOnSelf : MonoBehaviour
{
    public float SpeedRotation = 50f;
    bool _isActive = false;

    public void StartRotate()
    {
        _isActive = true;
    }

    public void StopRotate()
    {
        _isActive = false;
    }

    private void Update()
    {
        gameObject.transform.rotation *= Quaternion.Euler(0.0f, 0.0f, SpeedRotation * Time.deltaTime / Time.timeScale);
    }
}
