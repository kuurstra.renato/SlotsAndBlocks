﻿using System;
using UnityEngine;

public class InventoryView : MonoBehaviour
{
	public Camera _camera;
	
	Inventory _inventory;
	GameObject[] _blockViews;
	float _cameraTargetX;
	GameObject _currentObject;

	static Quaternion DEFAULT_ROTATION = Quaternion.Euler(-30, 30, 0);

	public void Setup(Inventory inv)
	{
		_inventory = inv;
		_blockViews = new GameObject[inv.blockCount];

		var pos = new Vector3(0f, 0f, 10f);

		for (int i = 0; i < inv.blockCount; ++i)
		{
			var slot = inv.GetSlot(i);
			var obj = Instantiate(slot.prefab);

			obj.transform.parent = transform;

			obj.transform.localPosition = pos - new Vector3(0,5,0);
			obj.transform.localRotation = DEFAULT_ROTATION;
			obj.tag = "Untagged"; // Don't register to activable group so the inventory doesn't go haywire on play
			Util.SetLayerRecursively(obj, LayerMask.NameToLayer("3D UI"));
			_blockViews[i] = obj;

			// Normalize scale
			Bounds b = Util.CalculateBounds(obj);
			float scale = 35f / (b.max - b.min).magnitude;
			obj.transform.localScale = new Vector3(scale, scale, scale);
			// We should normalize position too but fuck it

			pos.x += 2f;
		}

		OnCurrentBlockChanged();
	}

	public void OnCurrentBlockChanged()
	{
		if(_currentObject != null)
			_currentObject.transform.localRotation = DEFAULT_ROTATION;

		var i = _inventory.currentIndex;

		var view = _blockViews[i];
		_currentObject = view;

		_cameraTargetX = view.transform.localPosition.x;
	}

	private void Update()
	{
		float dt = Time.deltaTime / Time.timeScale;

		var pos = _camera.gameObject.transform.localPosition;
		pos.x = Mathf.Lerp(pos.x, _cameraTargetX, dt * 20f);
		_camera.gameObject.transform.localPosition = pos;

		_currentObject.transform.Rotate(Vector3.up, dt * 360f);
	}

	internal void SetVisible(bool v)
	{
		gameObject.SetActive(v);
	}
}

