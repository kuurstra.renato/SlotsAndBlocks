﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivableBlck
{

    public class EndOfTheWorldButton : Block
    {
        SphereCollider _collider;
        AudioSource bgm_AudioSource;
        public AudioClip FinalMusic;

        public Object[] Emitters;
        public float[] ConfettiShootAtTime;

        private void Awake()
        {
            _collider = GetComponent<SphereCollider>();
            bgm_AudioSource = GetComponent<AudioSource>();
            Emitters = Object.FindObjectsOfType(typeof(ConfettiFinale));
        }

        private void OnTriggerEnter(Collider other)
        {
            if(_IsPlaying == true && other.tag == "Blocks")
                GranFinale();
        }

        void GranFinale()
        {
            Object[] audioSources = Object.FindObjectsOfType(typeof(AudioSource));
            for (int i = 0; i < audioSources.Length; i++)
            {
                (audioSources[i] as AudioSource).Stop();
                bgm_AudioSource.clip = FinalMusic;
                bgm_AudioSource.Play();
                StartCoroutine(ConfettiShooter());
            }
        }

        public override void Play()
        {
            _IsPlaying = true;
        }

        public override void Stop()
        {
            _IsPlaying = false;
            for (int i = 0; i < Emitters.Length; i++)
            {
                ( Emitters[i] as ConfettiFinale).StopAndClear();
            }
            bgm_AudioSource.Stop();
        }

        IEnumerator ConfettiShooter()
        {
            yield return new WaitForSeconds( 1.0f);

            while (true)
            {
                shootConfetti();
                yield return new WaitForSeconds(Random.Range(0.8f, 1.0f));
            }

            //StopShootConfetti();

        }

        void shootConfetti()
        {
            for (int i = 0; i < Emitters.Length; i++)
            {
                (Emitters[i] as ConfettiFinale).Play();
            }
        }
        void StopShootConfetti()
        {
            for (int i = 0; i < Emitters.Length; i++)
            {
                (Emitters[i] as ConfettiFinale).Stop();
            }
        }
    }

}
