﻿using ActivableBlck;
using UnityEngine;

public class SimulationTrigger : MonoBehaviour
{
	public AudioClip playSound;
	public AudioClip stopSound;

	bool _simulating;
	AudioSource _audio;

	public bool isSimulating { get { return _simulating; } }

	private void Awake()
	{
		_audio = GetComponent<AudioSource>();
	}

	public void TogglePlay()
	{
		if (_simulating)
			Stop();
		else
			Play();
	}

	public void Play()
	{
		if (_simulating)
			return;

		_audio.clip = playSound;
		_audio.Play();

		Debug.Log("Simulate!");

		var blocks = GameObject.FindGameObjectsWithTag("Blocks");
		foreach (var block in blocks)
		{
			var b = block.GetComponent<Block>();
			if (b != null && b.gameObject.activeSelf)
				b.Play();
		}

		_simulating = true;
	}

	public void Stop()
	{
		if (!_simulating)
			return;

		_audio.clip = stopSound;
		_audio.Play();

		Debug.Log("Freeze all motor functions");

		var blocks = GameObject.FindGameObjectsWithTag("Blocks");
		foreach (var block in blocks)
		{
			var b = block.GetComponent<Block>();
			if (b != null && b.gameObject.activeSelf)
				b.Stop();
		}

		_simulating = false;
	}
}
