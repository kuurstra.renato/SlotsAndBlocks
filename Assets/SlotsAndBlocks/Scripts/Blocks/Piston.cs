﻿using UnityEngine;

public class Piston : MonoBehaviour
{
	public AudioClip _bounceSound;

	Animation _anim;
	AudioSource _audio;

	void Awake()
	{
		_anim = GetComponentInChildren<Animation>();
		_audio = GetComponent<AudioSource>();
	}

	private void OnTriggerEnter(Collider other)
	{
		_anim.Play("PistonAnim");

		Rigidbody rb = other.GetComponent<Rigidbody>();
		if (rb == null)
			rb = other.GetComponentInParent<Rigidbody>();

		if(rb != null)
		{
			rb.AddForce(transform.up * 15f, ForceMode.Impulse);
		}

		_audio.clip = _bounceSound;
		_audio.Play();
	}
}
