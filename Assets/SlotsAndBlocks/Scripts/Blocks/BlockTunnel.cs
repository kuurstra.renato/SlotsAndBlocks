﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivableBlck
{
    [RequireComponent(typeof(BoxCollider))]
    public class BlockTunnel : Block
    {
        public float Force = 10f;
        BoxCollider _boxCollider;

        private void Awake()
        {
        }

        public override void Play()
        {
        }
        public override void Stop()
        {
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.tag != "Blocks")
                return;

            Rigidbody rb = other.attachedRigidbody;
            rb.AddForce(-gameObject.transform.right * Force, ForceMode.Force);
        }
    }
}

