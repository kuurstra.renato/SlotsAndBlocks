﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ActivableBlck
{
    public class BlockWindMill : Block
    {
        public float SpeedRotation;
        RotateOnSelf _rotate;

        private void Awake()
        {
            _rotate = GetComponentInChildren<RotateOnSelf>();
            _rotate.SpeedRotation = SpeedRotation;
        }
    }
}
