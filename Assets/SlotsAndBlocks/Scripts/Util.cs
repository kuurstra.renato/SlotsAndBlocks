﻿using UnityEngine;

public static class Util
{
	public static void SetLayerRecursively(GameObject obj, int layer)
	{
		obj.layer = layer;
		for (int i = 0; i < obj.transform.childCount; ++i)
		{
			var child = obj.transform.GetChild(i);
			SetLayerRecursively(child.gameObject, layer);
		}
	}

	public static GameObject GetTagInParent(GameObject obj, string tagName)
	{
		if (obj == null)
			return null;
		if (obj.tag == tagName)
			return obj;
		if(obj.transform.parent != null)
			return GetTagInParent(obj.transform.parent.gameObject, tagName);
		else
			return null;
	}

	public static Bounds CalculateBounds(GameObject obj, Bounds bounds = new Bounds())
	{
		var r = obj.GetComponent<Renderer>();
		if (r != null)
		{
			bounds.Encapsulate(r.bounds);
		}
		var t = obj.transform;
		for (int i = 0; i < t.childCount; ++i)
		{
			var child = t.GetChild(i);
			bounds = CalculateBounds(child.gameObject, bounds);
		}
		return bounds;
	}

}

